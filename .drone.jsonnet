local Pipeline(name, image) = {
  kind: "pipeline",
  type: "docker",
  name: name,
  steps: [
    {
      name: "test",
      image: image,
      commands: [
        "python --version",
        "pip --version",
        "pip install nose coverage",
        "pip install .",
        "nosetests",
      ],
    },
  ]
};

[
  Pipeline("python-2-7", "python:2.7"),
  Pipeline("python-3-5", "python:3.5"),
  Pipeline("python-3-6", "python:3.6"),
  Pipeline("python-3-7", "python:3.7"),
  Pipeline("python-3-8", "python:3.8"),
  Pipeline("pypy-2-7", "pypy:2.7"),
  Pipeline("pypy-3", "pypy:3"),
]
